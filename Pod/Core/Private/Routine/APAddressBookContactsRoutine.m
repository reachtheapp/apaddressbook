//
//  APAddressBookContactsRoutine 
//  AddressBook
//
//  Created by Alexey Belkevich on 21.09.15.
//  Copyright © 2015 alterplay. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import "APAddressBookContactsRoutine.h"
#import "APAddressBookRefWrapper.h"
#import "APContactBuilder.h"
#import "APImageExtractor.h"
#import "APContact.h"

@interface APAddressBookContactsRoutine ()
@property (nonatomic, strong) APContactBuilder *builder;
@end

@implementation APAddressBookContactsRoutine

#pragma mark - life cycle

- (instancetype)initWithAddressBookRefWrapper:(APAddressBookRefWrapper *)wrapper
{
    self = [super initWithAddressBookRefWrapper:wrapper];
    self.builder = [[APContactBuilder alloc] init];
    return self;
}

#pragma mark - public

- (NSArray *)allContactsWithContactFieldMask:(APContactField)fieldMask groupMask:(NSArray *)groupMask
{
    NSMutableArray *contacts = [[NSMutableArray alloc] init];
    if (!self.wrapper.error)
    {
        if (groupMask == nil)
        {
            CFArrayRef peopleArrayRef = ABAddressBookCopyArrayOfAllPeople(self.wrapper.ref);
            
            CFIndex count = CFArrayGetCount(peopleArrayRef);
            for (CFIndex i = 0; i < count; i++)
            {
                ABRecordRef recordRef = CFArrayGetValueAtIndex(peopleArrayRef, i);
                APContact *contact = [self.builder contactWithRecordRef:recordRef fieldMask:fieldMask];
                [contacts addObject:contact];
            }
            CFRelease(peopleArrayRef);
        } else {
            CFArrayRef groups = ABAddressBookCopyArrayOfAllGroups(self.wrapper.ref);
            CFIndex numGroups = CFArrayGetCount(groups);
            for (int i=0; i<groupMask.count; i++)
            {
                int groupID = [((NSNumber *)[groupMask objectAtIndex:i]) intValue];
                if (groupID >= 0 && groupID < (int)numGroups)
                {
                    ABRecordRef groupRef = CFArrayGetValueAtIndex(groups, groupID);
                    
                    CFArrayRef members = ABGroupCopyArrayOfAllMembers(groupRef);
                    if(members) {
                        NSUInteger count = CFArrayGetCount(members);
                        for(NSUInteger idx=0; idx<count; ++idx) {
                            ABRecordRef recordRef = CFArrayGetValueAtIndex(members, idx);
                            APContact *contact = [self.builder contactWithRecordRef:recordRef fieldMask:fieldMask];
                            [contacts addObject:contact];
                        }
                        CFRelease(members);
                    }
                    
                    
                }
            }
            CFRelease(groups);
        }
    }
    return contacts.count > 0 ? contacts.copy : nil;
}

- (APContact *)contactByRecordID:(NSNumber *)recordID withFieldMask:(APContactField)fieldMask
{
    if (!self.wrapper.error)
    {
        ABRecordRef recordRef = ABAddressBookGetPersonWithRecordID(self.wrapper.ref, recordID.intValue);
        return recordRef != NULL ? [self.builder contactWithRecordRef:recordRef fieldMask:fieldMask] : nil;
    }
    return nil;
}

- (UIImage *)imageWithRecordID:(NSNumber *)recordID
{
    if (!self.wrapper.error)
    {
        ABRecordRef recordRef = ABAddressBookGetPersonWithRecordID(self.wrapper.ref, recordID.intValue);
        return recordRef != NULL ? [APImageExtractor photoWithRecordRef:recordRef] : nil;
    }
    return nil;
}

@end